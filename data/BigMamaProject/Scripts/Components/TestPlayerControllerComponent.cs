using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "ebb91033864417f686762eddbab062989f848517")]
public class TestPlayerControllerComponent : Component
{
	private GameUI gameUi;
	
	private void Init()
	{
		// write here code to be called on component initialization
		gameUi = new GameUI();
	}
	
	private void Update()
	{
		// write here code to be called before updating each render frame
		
	}
}