using System.Collections.Generic;
using System.Timers;
using Unigine;

[Component(PropertyGuid = "cb3c0d9cd023cc4b04f9352ef9c4fab5e300cc90")]
public class GuiController
{
    private float time = 0.0f;

    private static List<BaseGui> Pages { get; } = new List<BaseGui>();


    public GuiController()
    {
    }

    public static void Update()
    {
        foreach (var baseGui in Pages)
        {
            baseGui.Update();
        }
    }

    public static void WorldShutdown()
    {
        ClearPages();
    }

    private static void ClearPages()
    {
        for (var i = Gui.Get().NumChildren - 1; i > -1; --i)
        {
            Gui.Get().RemoveChild(Gui.Get().GetChild(i));
        }
        Pages.Clear();
    }

    public static void AddPage(BaseGui page)
    {
        Pages.Add(page);
    }

    public static BaseGui FindPageByName(string name)
    {
        return Pages.Find(gui => gui.GetPageLink() == name);
    }

    public void RunCounter()
    {
        var gui = Gui.Get();

        var ui = new UserInterface(gui, "MagicFirst/UI/main.ui");
        var window2 = ui.GetWidget(ui.FindWidget("Window"));
        ui.GetWidget(0);
        window2.Arrange();
        gui.AddChild(window2);
        var text = ui.GetWidget(ui.FindWidget("Text"));

        var timer = new Timer {Interval = 1000.0, Enabled = true};
        timer.Elapsed += (sender, args) =>
        {
            time += 1.0f;
            ((WidgetLabel) text).Text = $"{time}";
        };
    }
}